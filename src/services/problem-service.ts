import { Attempt } from "@/types/Attempt";
import { Problem } from "@/types/Problem";
// import http from "http"
import http from "@/services/http-common";
import { addTestCaseAsync } from "./testcase-service";
import { TestCase } from "@/types/TestCase";
import { TestCaseResult } from "@/types/TestCaseResult";

const api = "https://contester.azurewebsites.net/";

export async function getProblemsAsync(): Promise<Array<Problem>> {
  return (await http.get<Array<Problem>>("problems/all")).data    
}


export async function getProblemAttemptsAsync(id:number): Promise<Array<Attempt>> {
    return (await http.get<Array<Attempt>>(api +"problems/"+id+ "/attempts")).data    
  }

export async function getProblemAsync(id: number): Promise<Problem> {
  return (await http.get<Problem>("problems/" + id)).data
}
export async function getAttemptAsync(id: number): Promise<Attempt> {
  return (await http.get<Attempt>("attempts/" + id)).data
}
export async function getProblemTestcasesAsync(id: number): Promise<Array<TestCase>> {
  return (await http.get<Array<TestCase>>("problems/" + id + "/testcases")).data
}

export async function getAttemptTestcaseResultsAsync(id?: number): Promise<Array<TestCaseResult>> {
  return (await http.get<Array<TestCaseResult>>("attempts/" + id + "/testcaseresults")).data
}

export async function addProblemAsync(problem: Problem) {

    var id = (await http.post("problems", {
            "title": problem.title,
            "body": problem.body,
            "inputDescription": problem.inputDescription,
            "outputDescription": problem.outputDescription,
            "note": problem.note,
            "tags": problem.tags
    })).data;

    if(problem.testcases.length > 0){
        console.log("Adding tests");
        addTestCaseAsync(problem.testcases, id);
        console.log("Ending add tests");
    }
}

export async function sendAttemptAsync(attempt: Attempt) {
  http.post<Attempt>("attempts", attempt)
}
