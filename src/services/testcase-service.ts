import { TestCase } from './../types/TestCase';
import http from './http-common';

export async function addTestCaseAsync(testcases:Array<TestCase>,id:Number) {
    for (var i = 0; i < testcases.length; i += 1){
        await http.post("testcase", {
            "problemId": id,
            "input": testcases[i].input,
            "ExpectedOutput": testcases[i].expectedOutput,
        });
    }
}
