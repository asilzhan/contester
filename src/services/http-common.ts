import axios from "axios";
import { useUserStore } from "@/stores/user-store";
const api = "https://contester.azurewebsites.net/";
const tokenKey = "user-store:token"
const userstore = useUserStore();

const http = axios.create({
    baseURL: api,
    headers: {
        Authorization: !!userstore.token ? "Bearer " + userstore.token : ""
    }
})

export default http;
