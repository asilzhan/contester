import http from "@/services/http-common";
import { CurrentUser, LoginModel, RegistrationModel } from "@/types/RegistrationModel";

export async function addUser(user: RegistrationModel) {
    await http.post("users", user);
}

export async function loginAsync(loginModel: LoginModel): Promise<CurrentUser> {
    return (await http.post<CurrentUser>("auth/login", loginModel)).data;
}
